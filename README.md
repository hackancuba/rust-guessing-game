# Guessing Game in Rust

This is just the _guessing game_ from the Rust lang book, with a twist. Part of my journey into Rust.

Run with: `cargo run`.
