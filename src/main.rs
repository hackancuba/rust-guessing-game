use std::cmp::Ordering;
use std::io::stdin;

use rand::Rng;

fn main() {
    println!("Guess the number between 1 and 100 inclusive");
    println!();

    let secret_number: u8 = rand::thread_rng().gen_range(1..=100);
    let mut count: u8 = 0;

    loop {
        println!("Please input your guess:");
        let mut guess = String::new();
        stdin().read_line(&mut guess).expect("Failed to read line");

        let guess: u8 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You guessed: {guess}");
        count += 1;

        match guess.cmp(&secret_number) {
            Ordering::Less => {
                if guess.abs_diff(secret_number) <= 5 {
                    println!("Close, but too small!");
                } else {
                    println!("Too small!");
                }
            }
            Ordering::Greater => {
                if guess.abs_diff(secret_number) <= 5 {
                    println!("Close, but too big!");
                } else {
                    println!("Too big!");
                }
            }
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
    println!("Congratulations! You did it in {count} attempts")
}
